class CreateFriendships < ActiveRecord::Migration[5.1]
  def change
    create_table :friendships do |t|
      t.integer :requester_id, null: false
      t.integer :requested_id, null: false
      t.boolean :confirmed, null: false, default: true
      t.timestamps
    end
  end
end
