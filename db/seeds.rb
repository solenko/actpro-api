# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

%w(
lenka.3006
smiritel
nikitafeltov
inf1nity666
denisoddom
vad-551
anna.birdy1
egorovigor67
).each do |username|
  User.create(username: username, password: username, email: "#{username}@example.com")
end
