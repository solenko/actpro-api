class AuthToken < ApplicationRecord
  belongs_to :user

  scope :fresh, -> { where('expires_at > ?', Time.now) }

  has_secure_token :token

  def initialize(args = nil)
    super(args)
    refresh if self.expires_at.blank?
  end

  def refresh
    self.expires_at = 1.hour.from_now
  end

  def refresh!
    refresh
    save!
  end

end
