class User < ApplicationRecord
  has_many :auth_tokens, dependent: :delete_all
  has_many :friendships
end
