class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordInvalid, with: :render_ar_errors
  rescue_from ActionController::BadRequest, with: -> { render :head, status: :bad_request }
  after_filter :refresh_current_token

  def render_ar_errors(exception)
    render json: {errors: exception.record.errors.as_json}, status: :bad_request
  end

  def require_user!
    raise ActionController::Forbidden
  end

  def current_user
    token.try(:user)
  end

  def current_token
    @token ||= AuthToken.where(token: request.headers['X-Auth-Token']).first
  end

  def refresh_current_token
    current_token.refresh! if current_token
  end
end
