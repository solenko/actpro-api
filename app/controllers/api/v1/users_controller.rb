module Api
  module V1
    class UsersController < ::ApplicationController
      def index
        page = params.has_key?(:page) ? params[:page].to_i : 1
        raise ActionController::BadRequest.new if page < 1
        per_page = params.has_key?(:per_page) ? params[:per_page].to_i : 10
        raise ActionController::BadRequest.new if per_page < 1 || per_page > 30
        users = User.limit(per_page).offset((page - 1) * per_page)
        render json: users.as_json(only: [:id, :username])
      end

      def create
        user = User.create!(create_user_params)
        render json: user.as_json(only: [:id, :username, :email, :password])
      end

      def show
        user = User.find(params[:id])
        except = [:created_at, :updated_at]
        except << :password unless curent_user == user
        render json: user.as_json(except: except)
      end

      def sign_in
        u = User.where(username: params[:username], password: params[:password]).first!
        token = u.auth_tokens.fresh.first_or_create
        render json: {auth_token: token.token}
      end

      def create_user_params
        params.permit(:username, :email, :password)
      end

    end

  end
end

