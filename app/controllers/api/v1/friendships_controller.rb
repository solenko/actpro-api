module Api
  module V1
    class FriendshipsController < ::ApplicationController
      def create
        p = params[:friendship].permit!
        p[:requester_id] ||= current_user.id
        Frieldship.create(p)
      end
    end

  end
end
